﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ScheduleEmailSender.CornJobs.Configuration;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduleEmailSender.Services
{
    public class MyCronJob : CronJobService
    {
        private readonly ILogger<MyCronJob> _logger;
        private Producer _producer;

        public MyCronJob(IScheduleConfig<MyCronJob> config, ILogger<MyCronJob> logger, Producer producer)
            : base(config.CronExpression, config.TimeZoneInfo)
        {
            _logger = logger;
            _producer = producer;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("CronJob starts.");
            return base.StartAsync(cancellationToken);
        }

        public override Task DoWork(CancellationToken cancellationToken)
        {
            _logger.LogInformation($"{DateTime.Now:hh:mm:ss} Email sended.");
            Log.Logger = new LoggerConfiguration()
                .WriteTo.File("logs/logs.txt", restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Verbose)
                .CreateLogger();
            return Task.CompletedTask;
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("CronJob is stopping.");
            return base.StopAsync(cancellationToken);
        }
    }
}
