﻿using Microsoft.Extensions.Configuration;
using ScheduleEmailSender.CornJobs.Configuration;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;

namespace ScheduleEmailSender.Services
{
    public class Producer
    {
        public IConfiguration Configuration { get; }
        private static EmailSenderConfiguration _senderOptions;

        public Producer(IConfiguration configuration)
        {
            Configuration = configuration;
            _senderOptions = Configuration.GetSection("SenderOptions").Get<EmailSenderConfiguration>();
        }

        public void GmailSender()
        {
            List<Thread> threads = new List<Thread>();
            for (int i = 0; i < 20; i++)
            {
                int temp = i;
                Thread t = new Thread(() =>
                {
                    SendGmail($"Hello, there! {temp + 1}");
                });

                threads.Add(t);
            }

            for (int i = 0; i < threads.Count; i++)
            {
                threads[i].Start();
                // threads.Remove(threads[i]);
                Thread.Sleep(1000);
            }
        }

        public void SendGmail(string MessageBody)
        {
            string to, from, pass;
            MailMessage message = new MailMessage();
            to = "xchillds@gmail.com";
            from = _senderOptions.Email;
            pass = _senderOptions.Password;
            message.To.Add(to);
            message.From = new MailAddress(from);
            message.Body = MessageBody;
            message.Subject = "Sign Up to QuizSystem";
            message.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            smtp.EnableSsl = true;
            smtp.Port = 587;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Credentials = new NetworkCredential(from, pass);

            try
            {
                smtp.Send(message);
            }
            catch (Exception ex)
            {
                throw new Exception($"Something Happened Wrong While Sending Email, {ex}");
            }
        }
    }
}
