﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AE.Net.Mail;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ScheduleEmailSender.CornJobs.Configuration;

namespace ScheduleEmailSender.Services
{
    public class Consumer : CronJobService
    {
        static ImapClient client;
        private readonly ILogger<Consumer> _logger;

        public IConfiguration Configuration { get; }
        private static EmailSenderConfiguration _consumerOptions;

        public Consumer(IScheduleConfig<Consumer> config, ILogger<Consumer> logger, IConfiguration configuration)
            : base(config.CronExpression, config.TimeZoneInfo)
        {
            Configuration = configuration;
            _consumerOptions = Configuration.GetSection("ConsumerOptions").Get<EmailSenderConfiguration>();
            _logger = logger;
        }

        public void GmailReader()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            client = new ImapClient("imap.gmail.com", _consumerOptions.Email, _consumerOptions.Password, AuthMethods.Login, 993, true);

            client.SelectMailbox("INBOX");

            var messages = new List<MailMessage>();

            int uid = client.GetMessageCount()-1;

            for (int i = 0; i < 20; i++)
            {
                messages.Add(client.GetMessage(uid - i));
            }

            foreach (var message in messages)
            {
                Console.WriteLine(message?.Body);
                _logger.LogInformation($"Email consumed with {message.Subject} - subject, body: {message.Body}");
            }
         }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("Mail Consumer Starts.");
            return base.StartAsync(cancellationToken);
        }

        public override Task DoWork(CancellationToken cancellationToken)
        {
            GmailReader();
            return Task.CompletedTask;
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("Mail Consumer Is Stopping");
            return base.StopAsync(cancellationToken);
        }

    }
}
