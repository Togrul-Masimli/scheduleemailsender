﻿using System;

namespace ScheduleEmailSender.CornJobs.Configuration
{
    public interface IScheduleConfig<T>
    {
        string CronExpression { get; set; }
        TimeZoneInfo TimeZoneInfo { get; set; }
    }
}
